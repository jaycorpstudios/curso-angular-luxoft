angular.module('myapp', ['ngAnimate', 'ngFx', 'LocalStorageModule'])

    .config(['localStorageServiceProvider', function (localStorageServiceProvider) {
        localStorageServiceProvider
            .setPrefix('myapp')
            .setStorageType('localStorage')
            .setNotify(true, true)
    }])

    .controller('NotesController', ['$scope', '$http', 'localStorageService', function($scope, $http, localStorageService) {

        $scope.notes = [];
        $scope.sections = [];
        $scope.sectionSelected = localStorageService.get('currentSection');
        $scope.showNewSectionInput = false;
        $scope.hasNotes = false;
        $scope.order = 'sort';

        $scope.updateNotes = function() {
            var values = {
                'filterBy' : $scope.sectionSelected,
                'orderBy' : $scope.order
            };
            $http.get('/notes', { params : values })
                .success(function(notes) {
                    $scope.notes = notes;
                    $scope.hasNotes = $scope.notes.length > 0;
                });
        };

        $scope.$watch('order',function(){
            $scope.updateNotes();
        });


        $scope.addNote = function() {
            var note = {
                    text: $scope.text,
                    section: $scope.sectionSelected
                };
            $http.post('/notes', note)
                .success(function() {
                    $scope.text = '';
                    $scope.updateNotes();
                });
        };

        $scope.removeNote = function(noteId){
            $http.delete('/notes', { params: {id: noteId} }).success(function(){
                $scope.updateNotes();
            });
        };

        $scope.addNewSection = function(section){
            $http.post('/sections', {title: section})
                .success(function() {
                    $scope.newSection = '';
                    $scope.updateSections();
                    $scope.showNewSectionInput = false;
                });

        };

        $scope.updateSections = function() {
            $http.get('/sections')
                .success(function(sections) {
                    $scope.sections = sections;
                    //If no sections then create a default one
                    if($scope.sections.length === 0){
                        $scope.selectSection({title: 'Default'});
                        $scope.addNewSection('Default');
                    }
                });
        };

        $scope.selectSection =function(section){
            $scope.sectionSelected = ($scope.sectionSelected !== section.title) ? section.title : undefined;
            localStorageService.set('currentSection', $scope.sectionSelected);
            $scope.updateNotes();
        };

        $scope.showNewSection = function(value){
          $scope.showNewSectionInput = value;
        };

        $scope.orderBy = function(order){

        };

        $scope.updateSections();
        $scope.updateNotes();


    }]);
