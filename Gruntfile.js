module.exports = function (grunt) {

    //require('load-grunt-tasks')(grunt);
    grunt.loadNpmTasks('grunt-wiredep');

    grunt.initConfig({
        wiredep: {
            task: {
                src: ['public/index.html'],
                options: {}
            }
        }
    });

    grunt.registerTask('default', ['wiredep']);
};