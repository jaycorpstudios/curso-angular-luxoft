var express = require('express'),
    session = require('express-session'),
    bodyParser = require('body-parser'),
    MongoStore = require('connect-mongo')(session),
    fs = require('fs'),
    Db = require('mongodb').Db,
    Server = require('mongodb').Server,
    ObjectID = require('mongodb').ObjectID;

var db = new Db('tutor',
                new Server("localhost", 27017, {safe: true},
                {auto_reconnect: true}, {})
               );

db.open(function(){
    console.log("mongo db is opened!");
    db.collection('notes', function(error, notes) {
        db.notes = notes;
    });
    db.collection('sections', function(error, sections) {
        db.sections = sections;
    });
});

app = express(),
    path = require('path'),
    port = 3000;

app.use(express.static(path.join(__dirname, '/public')));
app.use('/bower_components',  express.static(__dirname + '/bower_components'));

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(session({
    store: new MongoStore({
        url: 'mongodb://localhost:27017/angular_session'
    }),
    secret: 'angular_tutorial',
    resave: true,
    saveUninitialized: true
}));

app.get('/notes', function(req, res) {
    var params = req.query,
        sortParams = {};
        sortParams[params.orderBy] = 1;
    var filterParams = (params.filterBy) ? {section: params.filterBy} : undefined;

    db.notes.find(filterParams).sort(sortParams).toArray(function(err, items) {
        res.send(items);
    });
});

app.post('/notes', function(req, res) {
    var note = req.body;
    note.date = (new Date()).getTime();

    db.notes.find().count().then(function(total){
        note.sort = total + 1;
        db.notes.insert(note);
        res.end();
    });
});

app.delete('/notes', function(req, res) {
    var id = new ObjectID(req.query.id);
    db.notes.remove({_id: id}, function(err){
        if (err) {
            console.log(err);
            res.send("Failed");
        } else {
            res.send("Success");
        }
    });
});

app.get("/sections", function(req,res) {
    db.sections.find(req.query).toArray(function(err, items) {
        res.send(items);
    });
});

app.post('/sections', function(req, res) {
    var section = req.body;
    db.sections.insert(section);
    res.end();
});


console.log('Connected to localhost:' + port);

app.listen(port);